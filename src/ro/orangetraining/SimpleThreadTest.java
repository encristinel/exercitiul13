package ro.orangetraining;

public class SimpleThreadTest {

    public static void main(String[] args) {
        new SimpleThread("Pleasent Gardens Hotel").start();
        new SimpleThread("Daydream Hotel").start();
        new SimpleThread("Royal Arc Resort & Spa").start();
    }
}
